package com.example.numbers

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener , TextWatcher {

    override fun afterTextChanged(s: Editable?) {

                                                }//afterTextChanged

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                                                                                         }//beforeTextChanged

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        if (!s.toString().isEmpty()) {
            OK.isClickable = true
            Clear.isClickable = true
                                     }//if statement

        if (OK.isClickable)
            OK.callOnClick()

        if (s.toString().isEmpty()) {

            Clear.callOnClick()
            Clear.isClickable = false

                                    }//if statement
                                                                                      }//onTextChanged

    override fun onClick(v: View?) {

        if (v!!.id == OK.id && !number.text.toString().isEmpty()){

            var longNumber : Long = number.text.toString().toLong()
            var numberAsString : String = number.text.toString()
            var reversedNumberAsString : String = numberAsString.reversed()
            var longReversedNumber : Long = reversedNumberAsString.toLong()
            var numberSqrt : Long = longNumber/2
            var longReversedNumberSqrt: Long = longReversedNumber/2

            var primeFlag : Int = 0
            var palindromeFlag : Int = 0
            var twinPrimeFlag : Int = 0

            for (i in 2 until numberSqrt-1)

                if (longNumber % i == 0L) {
                    primeFlag = 1
                    break
                                          }//if statement (to check if the number is prime or not)

            if (numberAsString != reversedNumberAsString)
                    palindromeFlag = 1

            if (primeFlag == 0) {
                for (i in 2 until longReversedNumberSqrt - 1)

                    if (longReversedNumber % i == 0L) {
                        twinPrimeFlag = 1
                        break
                                                      }//if statement (to check if the number is prime or not)
                                 }//if statement (to check if the number is twin prime or not)

            else if (primeFlag == 1)
                        twinPrimeFlag = 1

            if (primeFlag == 0) {
                prime.setTextColor(Color.GREEN)
                prime.text = "This number is a prime number"
                                }//if statement

            else {
                prime.setTextColor(Color.RED)
                prime.text = "This number is not a prime number"
                 }//else statement

            if (twinPrimeFlag == 0) {
                twinPrime.setTextColor(Color.GREEN)
                twinPrime.text = "This number is a twin prime number"
                                    }//if statement

            else {
                twinPrime.setTextColor(Color.RED)
                twinPrime.text = "This number is not a twin prime number"
                 }//else statement

            if (palindromeFlag == 0) {
                palindrome.setTextColor(Color.GREEN)
                palindrome.text = "This number is a palindrome number"
                                     }//if statement

            else {
                palindrome.setTextColor(Color.RED)
                palindrome.text = "This number is not a palindrome number"
                 }//else statement

                                                             }//if statement (Ok click Listener)

        else if (v!!.id == Clear.id){

            number.text.clear()

            prime.text = ""

            twinPrime.text = ""

            palindrome.text = ""

            OK.isClickable = false


                                     }//else if statement (clear click Listener)

                                    }//onClick Listener

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        OK.isClickable = false
        Clear.isClickable = false

        OK.setOnClickListener(this)
        Clear.setOnClickListener(this)
        number.addTextChangedListener(this)

                                                       }//onCreate

                                                                               }//MainActivity

